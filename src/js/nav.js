import wordsData from './wordsData';
import pubSub from './lib/pubsub';

const changeLanguage = (e) => {
    const clickedEl = e.currentTarget;
    const elLanguage = clickedEl.dataset.language;

    pubSub.emit('languageChanged', elLanguage);
};

const navElements = (language) => {
    const navSpan = document.createElement('div');

    navSpan.classList.add('main-nav-item');

    if (language === 'english') {
        navSpan.classList.add('active');
    }

    navSpan.setAttribute('data-language', language);
    navSpan.innerText = language;

    navSpan.addEventListener('click', changeLanguage);

    return navSpan;
};

const setActiveNavLanguage = (language) => {
    const navItemsArr = document.querySelectorAll('[data-language]');

    navItemsArr.forEach(el => {
        if (language === el.dataset.language) {
            el.classList.add('active');
        } else {
            el.classList.remove('active');
        }
    });
};

pubSub.on('languageChanged', setActiveNavLanguage);

export default () => {
    let navItems = document.createDocumentFragment();

    Object.keys(wordsData.languages).forEach(language => {
        const navEl = navElements(language)
        navItems.appendChild(navEl);
    });
    
    return navItems;
}