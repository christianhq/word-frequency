import wordsData from './wordsData';
import pubSub from './lib/pubsub';

const data = wordsData.wordList;
const wordCount = Object.keys(data).length;
let itemsPerPage = 100;
const pages = Math.ceil(wordCount / itemsPerPage);

const itemsLimitChange = (e) => {
    pubSub.emit('paginationLimitChanged', )
}

const paginate = (e) => {
    const optionSelected = e.target;
    pubSub.emit('pageChanged', optionSelected.value);
}

export default () => {
    const form = document.createElement('form');
    const select = document.createElement('select');
    console.log(`pages ${pages}`)
    for(let i = 0; i < pages; i++) {
        const currentPage = i + 1;
        const optionEl = document.createElement('option');
        optionEl.classList.add('pagination-option');
        optionEl.setAttribute('value', currentPage);

        optionEl.innerText = currentPage;

        select.appendChild(optionEl);
    }

    form.classList.add('pagination-form');
    select.classList.add('pagination-selector');

    select.setAttribute('data-paginationSelector', '');
    select.addEventListener('change', paginate);

    form.appendChild(select);

    return form;
}