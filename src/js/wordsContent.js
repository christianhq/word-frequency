import wordsData from './wordsData';
import pubSub from './lib/pubsub';

let language = 'english';
let page = 1;

const createTable = (keyLanguage, word) => {
    const fragment = document.createDocumentFragment()
    const tableRow = document.createElement('tr');
    const languageCell = document.createElement('td');
    const wordCell = document.createElement('td');

    tableRow.classList.add('word-card-table-row');
    languageCell.classList.add('word-card-language-cell');
    wordCell.classList.add('word-card-table-cell');

    languageCell.innerText = keyLanguage;
    wordCell.innerText = word;

    tableRow.appendChild(languageCell);
    tableRow.appendChild(wordCell);

    return fragment.appendChild(tableRow);
}

const wordElement = (key, data) => {
    const wordCard = document.createElement('div');
    const wordRank = document.createElement('span');
    const wordDiv = document.createElement('div');
    const wordsTable = document.createElement('table');

    wordCard.classList.add('word-card');
    wordRank.classList.add('word-card-rank');
    wordDiv.classList.add('word-card-main-word');
    wordsTable.classList.add('word-card-table');

    Object.keys(data).forEach(keyLanguage => {

        if (keyLanguage === language) {
            wordRank.innerText = key;
            wordDiv.innerText = data[keyLanguage];

        } else {
            wordsTable.appendChild(createTable(keyLanguage, data[keyLanguage]));
        }
    });

    wordCard.appendChild(wordRank);
    wordCard.appendChild(wordDiv);
    wordCard.appendChild(wordsTable);

    return wordCard;
};

const getPageData = () => {
    const currentPage = page;
    
    const pageLimit = currentPage * 100;
    let pageData = {};

    for (let i = (pageLimit - 100); i < pageLimit; i++) {
        if (wordsData.wordList[i]) {
            pageData[i] = wordsData.wordList[i];
        } else {
            break;
        }
    }
    
    return pageData;
}

const populateWords = () => {
    const fragment = document.createDocumentFragment();
    const data = getPageData();

    Object.keys(data).forEach(key => {
        const el = wordElement(key, data[key]);

        fragment.appendChild(el);
    });
        
    return fragment;
}

const populateChanges = () => {
    const wordElWrappers = document.querySelectorAll('[data-wordWrapper]');

    const wordsEl = populateWords();

    wordElWrappers.forEach(wrapEl => {
            wrapEl.innerHTML = '';

            wrapEl.appendChild(wordsEl);
        }
    );
}

const populateChangedLanguage = (languageChosen) => {
    language = languageChosen;

    populateChanges();
}

const pageChanged = (newPage) => {
    page = newPage;
    populateChanges();
}

pubSub.on('languageChanged', populateChangedLanguage);
pubSub.on('pageChanged', pageChanged)

export default () => {
    const wordElWrapper = document.createElement('div');

    wordElWrapper.classList.add('words-wrapper');
    wordElWrapper.setAttribute('data-wordWrapper', '');

    wordElWrapper.appendChild(populateWords());
    
    return wordElWrapper;
}