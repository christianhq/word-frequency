import nav from './nav';
import wordContent from './wordsContent';
import pagination from './pagination';

const mainNav = document.querySelector('[data-mainNav]');
const wordsContainer = document.querySelector('[data-wordsContainer]');
const paginationContainer = document.querySelector('[data-paginationContainer]');

mainNav.appendChild(nav());
wordsContainer.appendChild(wordContent());
paginationContainer.appendChild(pagination());